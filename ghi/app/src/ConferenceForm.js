import React, { useEffect, useState } from 'react';


function ConferenceForm() {
    const [locations, setLocations] = useState([]);
    const fetchData = async () => {
        const url = 'http://localhost:8000/api/locations/'

        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setLocations(data.locations);
        }
    }

    const [name, setName] = useState('')
    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }

    const [startDate, setStartDate] = useState('')
    const handleStartDateChange = (event) => {
        const value = event.target.value;
        setStartDate(value);
    }
    const [endDate, setEndDate] = useState('')
    const handleEndDateChange = (event) => {
        const value = event.target.value;
        setEndDate(value);
    }
    const [description, setDescription] = useState('')
    const handleDescriptionChange = (event) => {
        const value = event.target.value;
        setDescription(value);
    }
    const [maxAttendees, setMaxAttendees] = useState('')
    const handleMaxAttendeesChange = (event) => {
        const value = event.target.value;
        setMaxAttendees(value);
    }
    const [maxPresentations, setMaxPresentations] = useState('')
    const handleMaxPresentationsChange = (event) => {
        const value = event.target.value;
        setMaxPresentations(value);
    }
    const [location, setLocation] = useState('')
    const handleLocationChange = (event) => {
        const value = event.target.value;
        setLocation(value);
    }
    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};
        data.name = name;
        data.starts = startDate;
        data.ends = endDate;
        data.description = description;
        data.max_attendees = maxAttendees;
        data.max_presentations = maxPresentations;
        data.location = location;

        const conferenceUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(conferenceUrl, fetchConfig);
        if (response.ok) {
            const newConference = await response.json();
            console.log(newConference);
            setName('');
            setStartDate('');
            setEndDate('');
            setDescription('');
            setMaxAttendees('');
            setMaxPresentations('');
            setLocation('');
        }
    }





    useEffect(() => {
        fetchData();
      }, []);


    return (

      <div className="my-5 conatiner">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new conference</h1>
            <form onSubmit={handleSubmit} id="create-conference-form">
              <div className="form-floating mb-3">
                <input onChange={handleNameChange} placeholder="Name" required type="text" id="name" name="name" className="form-control" />
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleStartDateChange} placeholder="Starts" required type="date" id="starts"  name="starts" className="form-control" />
                <label htmlFor="starts">Start Date</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleEndDateChange}  placeholder="Ends" required type="date" id="ends" name="ends" className="form-control" />
                <label htmlFor="ends">End Date</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleDescriptionChange}  placeholder="Description" required type="textarea" id="description" name="description" className="form-control" />
                <label htmlFor="description">Description</label>
              </div>              <div className="form-floating mb-3">
                <input onChange={handleMaxAttendeesChange}  placeholder="Max Attendees" required type="number" id="max_attendees" name="max_attendees" className="form-control" />
                <label htmlFor="max-attendees">Maximum Number of Attendees</label>
              </div>              <div className="form-floating mb-3">
                <input onChange={handleMaxPresentationsChange}  placeholder="Max Presentations" required type="number" id="max_presentations" name="max_presentations" className="form-control" />
                <label htmlFor="max-presentations">Maximum Number of Presentations</label>
              </div>
              <div className="mb-3">
                <select onChange={handleLocationChange}  required id="location" name="location" className="form-select">
                  <option value=''>Choose a location</option>
                  {locations.map(location => {
                    return (
                        <option key={location.id} value={location.id}>
                            {location.name}
                        </option>
                    )
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>

    )
}

export default ConferenceForm;
